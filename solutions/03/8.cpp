#define _USE_MATH_DEFINES
#include <cmath>
#include <cstdio>
#include <functional>
#include <random>
#include <tuple>

using namespace std;

class BoxMuller
{
private:
	mt19937 gen;
	uniform_real_distribution<> dist;

	bool spare = false;
	double r1, r2;
	double s;

public:
	BoxMuller() : gen(), dist(-1, 1)
	{
	}

	double operator()()
	{
		if (spare)
		{
			spare = false;
			return r2 * sqrt(-2 * log(s) / s);
		}
		spare = true;

		do
		{
			r1 = dist(gen);
			r2 = dist(gen);
			s = r1 * r1 + r2 * r2;
		}
		while (s >= 1 || s == 0);

		return r1 * sqrt(-2 * log(s) / s);
	}
};

class CentralLimit
{
private:
	mt19937 gen;
	uniform_real_distribution<> dist;

	int N = 1000;

public:
	CentralLimit(): gen(), dist(0, 1)
	{
	}

	double operator()()
	{
		double s = 0;
		for (int i = 0; i < N; i++)
		{
			s += dist(gen);
		}

		return (s - double(N) / 2) / sqrt(double(N) / 12);
	}
};

class Neumann
{
private:
	mt19937 gen;
	uniform_real_distribution<> dist;

	function<double(double)> f;
	function<tuple<double, double>(mt19937&)> g;

public:
	Neumann(function<double(double)> f, function<tuple<double, double>(mt19937&)> g) : gen(), dist(0, 1), f(f), g(g)
	{
	}

	double operator()()
	{
		double x, gx, u;
		do
		{
			tie(x, gx) = g(gen);
			u = dist(gen);
		}
		while (u >= f(x) / gx);
		return x;
	}
};

void generate(function<double()> random, int N, const string& name)
{
	auto file = fopen((string("8-") + name + ".dat").c_str(), "w");
	for (int i = 0; i < N; i++)
	{
		fprintf(file, "%.16f\n", random());
	}
	fclose(file);
}

int main()
{
	int N = 1e6;

	generate(BoxMuller(), N, "bm");
	generate(CentralLimit(), N, "cl");

	uniform_real_distribution<> pi_dist(0, M_PI);
	generate(Neumann([](double x) { return 0.5 * sin(x); }, [&pi_dist](mt19937& gen) { return make_tuple(pi_dist(gen), 1); }), N, "neu");

	mt19937 gen{};
	uniform_real_distribution<> dist(0, 1);
	generate([&gen, &dist]() { return cbrt(dist(gen)); }, N, "tr");

	return 0;
}
