import sys

import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

f = sys.argv[1]

r = np.loadtxt(f + '.dat')

fig, ax = plt.subplots(1, 1)
ax.hist(r, bins=100)
fig.savefig(f + '-hist.pdf')

fig, ax = plt.subplots(1, 1)
ax.scatter(r[:-1:101], r[1::101])
fig.savefig(f + '-2d.pdf')

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1, projection='3d')
ax.scatter(r[:-2:101], r[1:-1:101], r[2::101])
ax.view_init(None, 30)
fig.savefig(f + '-3d.pdf')
