#include <cstdio>
#include <cstdlib>
#include <functional>
#include <random>
#include <string>

using namespace std;

class LCG
{
private:
	unsigned int a = 1601;
	unsigned int b = 3456;
	unsigned int m = 1e4;
	unsigned int x = 0;

public:
	double operator()()
	{
		x = (a * x + b) % m;
		return double(x) / double(m);
	}
};

void generate(function<double()> random, int N, const string& name)
{
	auto file = fopen((string("7-") + name + ".dat").c_str(), "w");
	for (int i = 0; i < N; i++)
	{
		fprintf(file, "%.16f\n", random());
	}
	fclose(file);
}

int main()
{
	int N = 1e6;
	generate(LCG(), N, "lcg");
	generate([](){ return double(rand()) / double(RAND_MAX); }, N, "rand");
	mt19937 gen;
	uniform_real_distribution<> dist(0, 1);
	generate(bind(dist, gen), N, "mt");
	return 0;
}
