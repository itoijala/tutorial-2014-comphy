import numpy as np
from scipy.stats import norm

import matplotlib as mpl
import matplotlib.pyplot as plt

r = np.loadtxt('8-bm.dat')

fig, ax = plt.subplots(1, 1)
ax.hist(r, bins=100, normed=True)
x = np.linspace(*ax.get_xlim(), num=1000)
ax.plot(x, norm.pdf(x), 'r-')
ax.set_ylim(0, ax.get_ylim()[1])
fig.savefig('8-bm.pdf')

r = np.loadtxt('8-cl.dat')

fig, ax = plt.subplots(1, 1)
ax.hist(r, bins=100, normed=True)
x = np.linspace(*ax.get_xlim(), num=1000)
ax.plot(x, norm.pdf(x), 'r-')
ax.set_ylim(0, ax.get_ylim()[1])
fig.savefig('8-cl.pdf')

r = np.loadtxt('8-neu.dat')

fig, ax = plt.subplots(1, 1)
ax.hist(r, bins=100, normed=True)
x = np.linspace(*ax.get_xlim(), num=1000)
ax.plot(x, np.sin(x) / 2, 'r-')
ax.set_ylim(0, ax.get_ylim()[1])
fig.savefig('8-neu.pdf')

r = np.loadtxt('8-tr.dat')

fig, ax = plt.subplots(1, 1)
ax.hist(r, bins=100, normed=True)
x = np.linspace(*ax.get_xlim(), num=1000)
ax.plot(x, x**2 * 3, 'r-')
ax.set_ylim(0, ax.get_ylim()[1])
fig.savefig('8-tr.pdf')
