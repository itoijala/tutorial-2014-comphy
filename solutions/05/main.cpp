#include <cmath>
#include <fstream>
#include <functional>
#include <iostream>
#include <random>

#include <omp.h>

#include <Eigen/Dense>

using namespace std;
using Eigen::ArrayXXi;

double E(double Jx, double Jy, const ArrayXXi& a, int x, int y)
{
	int Nx = a.rows();
	int Ny = a.cols();
	double E = 0;
	E -= Jy * a(x, y) * a(x, (y + 1 + Ny) % Ny);
	E -= Jy * a(x, y) * a(x, (y - 1 + Ny) % Ny);
	E -= Jx * a(x, y) * a((x + 1 + Nx) % Nx, y);
	E -= Jx * a(x, y) * a((x - 1 + Nx) % Nx, y);
	return E;
}

template<class URNG>
double mc_sweep(double T, double Jx, double Jy, ArrayXXi& a, URNG& rng)
{
	int Nx = a.rows();
	int Ny = a.cols();
	uniform_int_distribution<> randomNx(0, Nx - 1);
	uniform_int_distribution<> randomNy(0, Ny - 1);
	uniform_real_distribution<> randomR(0, 1);

	double dE_ges = 0;
	for (int n = 0; n < Nx * Ny; n++)
	{
		int x = randomNx(rng);
		int y = randomNy(rng);
		double dE = -2 * E(Jx, Jy, a, x, y);
		if (dE <= 0 || randomR(rng) < exp(- dE / T))
		{
			a(x, y) *= -1;
			dE_ges += dE;
		}
	}
	return dE_ges;
}

void mc(double T, double Jx, double Jy, function<ArrayXXi(int, int)> f, int Nx, int Ny, const char *name0, int sweeps)
{
	mt19937 gen;

	ArrayXXi a = f(Nx, Ny);

	double E_ges = 0;
	for (int x = 0; x < Nx; x++)
	{
		for (int y = 0; y < Ny; y++)
		{
			E_ges += 0.5 * E(Jx, Jy, a, x, y);
		}
	}

	vector<double> E(sweeps);
	for (int t = 0; t < sweeps; t++)
	{
		E_ges += mc_sweep(T, Jx, Jy, a, gen);
		E[t] = E_ges;
	}

	char name[256];
	sprintf(name, "a-%.2f-%s.csv", T, name0);
	FILE *file = fopen(name, "w");
	for (int t = 0; t < sweeps; t++)
	{
		fprintf(file, "%d %.20f\n", t, E[t] / (Nx * Ny));
	}
	fclose(file);

	sprintf(name, "b-%.2f-%s.csv", T, name0);
	ofstream of(name);
	of << a << endl;
	of.close();
}

ArrayXXi ones(int Nx, int Ny)
{
	return ArrayXXi::Ones(Nx, Ny);
}

ArrayXXi randoms(int Nx, int Ny)
{
	mt19937 gen;
	uniform_int_distribution<> dist(0, 1);
	return ArrayXXi(Nx, Ny).unaryExpr([&gen, &dist](int x) { return 2 * dist(gen) - 1; });
}

int main()
{
	Eigen::initParallel();

	double T[] = {1, 2.25, 3};
	#pragma omp parallel for
	for (int i = 0; i < 3; i++)
	{
		mc(T[i], 1, 1, ones, 100, 100, "ones", 1e4);
		mc(T[i], 1, 1, randoms, 100, 100, "randoms", 1e4);
		mc(T[i], 1, 0, randoms, 100, 1, "1d", 1e4);
	}

	return 0;
}
