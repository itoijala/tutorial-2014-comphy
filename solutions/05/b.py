import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

for T in [1, 2.25, 3]:
    for a0 in ["ones", "randoms"]:
        a = np.loadtxt("b-{:.2f}-{}.csv".format(T, a0))
        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)
        im = ax.matshow(a, vmin=-1, vmax=1)
        cb = fig.colorbar(im, ax=ax)
        fig.savefig("b-{:.2f}-{}.pdf".format(T, a0))
