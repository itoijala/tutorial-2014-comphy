import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

for T in [1, 2.25, 3]:
    t, E = np.loadtxt("a-{:.2f}-1d.csv".format(T), unpack=True)
    fig, ax = plt.subplots(1, 1)
    ax.plot(t, E)
    N = 100
    Z = (np.exp(1 / T) + np.exp(-1 / T))**N + (np.exp(1 / T) - np.exp(-1 / T))**N
    ax.axhline(y=(-1 / Z * ((np.exp(1 / T) - np.exp(-1 / T)) * (np.exp(1 / T) + np.exp(-1 / T))**(N - 1) + (np.exp(1 / T) + np.exp(-1 / T)) * (np.exp(1 / T) - np.exp(-1 / T))**(N - 1))), c='r')
    ax.set_xlabel("$t$")
    ax.set_ylabel('$E$')
    fig.savefig("c-{:.2f}.pdf".format(T))
