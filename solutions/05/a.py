import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

for T in [1, 2.25, 3]:
    for a0 in ["ones", "randoms"]:
        t1, E1 = np.loadtxt("a-{:.2f}-ones.csv".format(T), unpack=True)
        tr, Er = np.loadtxt("a-{:.2f}-randoms.csv".format(T), unpack=True)
        fig, ax = plt.subplots(1, 1)
        ax.plot(t1, E1)
        ax.plot(tr, Er)
        ax.set_xlabel("$t$")
        ax.set_ylabel('$E$')
        fig.savefig("a-{:.2f}.pdf".format(T))
