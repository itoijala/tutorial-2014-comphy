import numpy as np

import matplotlib as mpl
mpl.rcdefaults()
import matplotlib.pyplot as plt
import matplotlib.animation

psi = np.loadtxt('psi.csv')
xi = np.linspace(-10, 10, len(psi[0]))
t = 0.01 * np.arange(len(psi))

fig, ax = plt.subplots(1, 1)
ax.set_xlim(-10, 10)
ax.set_ylim(0, 0.1)
ax.set_xlabel(r'$\xi$')
ax.set_ylabel(r'$|\psi|^2$')
line, = ax.plot([], [])
title = ax.set_title('$t = 000.00$')
title.set_ha('left')
pos = title.get_position()

def init():
    line.set_data([], [])
    title.set_text('$t = 000.00$')
    title.set_position(pos)
    return line, title

def animate(i):
    line.set_data(xi, psi[i])
    title.set_text('$t = {: 3.2f}$'.format(t[i]))
    title.set_position(pos)
    return line, title

anim = mpl.animation.FuncAnimation(fig, animate, init_func=init, frames=len(t), blit=True)
anim.save('animation.mp4', fps=100, extra_args=['-vcodec', 'libx264'])
