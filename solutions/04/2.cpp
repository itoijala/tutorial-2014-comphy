#include <cmath>
#include <cstdio>
#include <functional>

double simpson(std::function<double(double)> f, double a, double b, unsigned int N)
{
	N *= 2;
	double h = (b - a) / N;
	double sum = 0;
	#pragma omp parallel
	{
		#pragma omp for reduction(+:sum)
		for (unsigned int i = 2; i < N; i += 2)
		{
			sum += 2 * f(a + h * i);
		}
		#pragma omp for reduction(+:sum)
		for (unsigned int i = 1; i < N; i += 2)
		{
			sum += 4 * f(a + h * i);
		}
	}
	sum += f(a) + f(b);
	sum *= h / 3;
	return sum;
}

double I(double q, unsigned int N)
{
	double u = simpson([N, q](double r)
	{
		return simpson([N, q, r](double phi)
		{
			return r * cos(q * r * cos(phi));
		}, 0, 2 * M_PI, N);
	}, 1, 2, 100);
	return u * u;
}

void b(unsigned int N)
{
	auto *file = fopen("2a.dat", "w");
	for (int n = 0; n <= 50; n++)
	{
		double q = 0.1 * n;
		fprintf(file, "%.15f %.15f\n", q, I(q, N));
	}
	fclose(file);
}

double I(double qx, double qy, double phi_max, unsigned int N)
{
	double q = sqrt(qx * qx + qy * qy);
	double alpha = atan2(qx, qy);

	double uRe = simpson([N, q, alpha, phi_max](double r)
	{
		return simpson([N, q, alpha, r](double phi)
		{
			return r * cos(q * r * cos(alpha - phi));
		}, 0, phi_max, N);
	}, 1, 2, 100);

	double uIm = simpson([N, q, alpha, phi_max](double r)
	{
		return simpson([N, q, alpha, r](double phi)
		{
			return r * sin(q * r * cos(alpha - phi));
		}, 0, phi_max, N);
	}, 1, 2, 100);

	return uRe * uRe + uIm * uIm;
}

void c(unsigned int N)
{
	double **Is = new double*[61];
	for (int m = -30; m <= 30; m++)
	{
		Is[m + 30] = new double[61];
		for (int n = -30; n <= 30; n++)
		{
			double qx = 0.2 * m;
			double qy = 0.2 * n;
			Is[m + 30][n + 30] = I(qx, qy, 0.5 * M_PI, N);
		}
	}

	auto *file = fopen("2b.dat", "w");
	for (int m = -30; m <= 30; m++)
	{
		for (int n = -30; n <= 30; n++)
		{
			double qx = 0.2 * m;
			double qy = 0.2 * n;
			fprintf(file, "%.15f %.15f %.15f\n", qx, qy, Is[m + 30][n + 30]);
		}
	}
	fclose(file);
}

int main()
{
	unsigned int N = 100;

	b(N);
	c(N);
}
