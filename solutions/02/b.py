import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
#import sys

#p = sys.argv[1]

for p in [0.1, 0.5, 0.9]:
    R = np.loadtxt("b-a-{:.1f}.csv".format(p))
    C = np.loadtxt("b-c-{:.1f}.csv".format(p))
    fig = plt.figure()
    ax1 = fig.add_subplot(1, 2, 1)
    ax2 = fig.add_subplot(1, 2, 2)
    ax1.matshow(R)
    ax2.matshow(C)
    fig.savefig("b-{:.1f}.pdf".format(p))
