#include <cstdio>
#include <fstream>
#include <functional>
#include <iostream>
#include <random>
#include <stack>
#include <tuple>

#include <omp.h>

#include <Eigen/Dense>

Eigen::ArrayXXi generateArray(int L, double p, std::function<double()> &ran)
{
	return Eigen::ArrayXXi::Zero(L, L).unaryExpr([&ran, p](int x){ return ran() < p ? 1 : 0; });
}

int cluster(const Eigen::ArrayXXi &realisation, Eigen::ArrayXXi &colouredClusters, int L, int x, int y, int colour)
{
	if (colouredClusters(x, y) != 0 || realisation(x, y) == 0)
	{
		return colour;
	}

	colour++;
	colouredClusters(x, y) = colour;
	std::stack<std::tuple<int, int>> s;
	s.push(std::make_tuple(x, y));

	while (!s.empty())
	{
		int x, y;
		std::tie(x, y) = s.top();
		s.pop();

		for (int i = -1; i <= 1; i += 2)
		{
			if (x + i >= 0 && x + i < L)
			{
				if (colouredClusters(x + i, y) == 0 && realisation(x + i, y) != 0)
				{
					colouredClusters(x + i, y) = colour;
					s.push(std::make_tuple(x + i, y));
				}
			}
			if (y + i >= 0 && y + i < L)
			{
				if (colouredClusters(x, y + i) == 0 && realisation(x, y + i) != 0)
				{
					colouredClusters(x, y + i) = colour;
					s.push(std::make_tuple(x, y + i));
				}
			}
		}
	}
	return colour;
}


void visualiseRealisation(int L, double p, std::function<double()> &ran)
{
	Eigen::ArrayXXi realisation = generateArray(L, p, ran);
	Eigen::ArrayXXi colouredClusters = Eigen::ArrayXXi::Zero(L, L);
	int colour = 1;

	for (int i = 0; i < L; i++)
	{
		for (int j = 0; j < L; j++)
		{
			colour = cluster(realisation, colouredClusters, L, i, j, colour);
		}
	}

	char name[256];
	std::sprintf(name, "b-a-%.1f.csv", p);
	std::ofstream f(name);
	f << realisation << std::endl;
	f.close();
	std::sprintf(name, "b-c-%.1f.csv", p);
	f.open(name);
	f << colouredClusters << std::endl;
	f.close();
}

std::tuple<bool, int> percolate(int L, double p, std::function<double()> &ran)
{
	Eigen::ArrayXXi array = generateArray(L, p, ran);
	Eigen::ArrayXXi c = Eigen::ArrayXXi::Zero(L, L);
	int colour = 1;
	int p_cluster = 0;

	for (int i = 0; i < L; i++)
	{
		int old = colour;
		colour = cluster(array, c, L, i, 0, colour);
		if (p_cluster == 0 && colour != old)
		{
			bool p1, p2, p3;
			p1 = p2 = p3 = false;
			for (int j = 0; j < L; j++)
			{
				if (c(0, j) == colour)
				{
					p1 = true;
					break;
				}
			}
			for (int j = 0; j < L; j++)
			{
				if (c(L - 1, j) == colour)
				{
					p2 = true;
					break;
				}
			}
			for (int j = 0; j < L; j++)
			{
				if (c(j, L - 1) == colour)
				{
					p3 = true;
					break;
				}
			}
			if (p1 && p2 && p3)
			{
				p_cluster = colour;
			}
		}
	}

	for (int i = 0; i < L; i++)
	{
		for (int j = 1; j < L; j++)
		{
			colour = cluster(array, c, L, i, j, colour);
		}
	}

	int *M = new int[colour + 1]();
	for (int i = 0; i < L; i++)
	{
		for (int j = 0; j < L; j++)
		{
			M[c(i, j)]++;
		}
	}

	int M_inf = 0;
	for (int i = 1; i <= colour; i++)
	{
		if (i != p_cluster && M[i] > M_inf)
		{
			M_inf = M[i];
		}
	}

	delete[] M;
	return std::make_tuple(p_cluster != 0, M_inf);
}

std::tuple<double, double> percolationProbability(int L, double p, int R)
{
	int percolating = 0;
	int M_inf = 0;

	#pragma omp parallel
	{
		std::mt19937 gen(omp_get_thread_num());
		std::uniform_real_distribution<double> dist(0, 1);
		std::function<double()> ran = bind(dist, gen);

		#pragma omp for
		for (int i = 0; i < R; i++)
		{
			bool tpercolating;
			int tM_inf;
			std::tie(tpercolating, tM_inf) = percolate(L, p, ran);
			percolating += tpercolating ? 1 : 0;
			M_inf += tM_inf;
		}
	}

	return std::make_tuple(double(percolating) / R, double(M_inf) / R / (L * L));
}

void avg(int L, int R)
{
	char name[256];
	std::sprintf(name, "c-%d.csv", L);
	auto file = fopen(name, "w");
	for (int i = 0; i < 100; i++)
	{
		double p = i / 100.0;
		double q, M_inf;
		std::tie(q, M_inf) = percolationProbability(L, p, R);
		fprintf(file, "%.20f %.20f %.20f\n", p, q, M_inf);
	}
	fclose(file);
}


int main()
{
	Eigen::initParallel();

	#pragma omp parallel
	{
		std::mt19937 gen(omp_get_thread_num());
		std::uniform_real_distribution<double> dist(0, 1);
		std::function<double()> ran = bind(dist, gen);

		#pragma omp sections
		{
			#pragma omp section
			{
				visualiseRealisation(50, 0.1, ran);
			}
			#pragma omp section
			{
				visualiseRealisation(50, 0.5, ran);
			}
			#pragma omp section
			{
				visualiseRealisation(50, 0.9, ran);
			}
		}
	}

	avg( 10, 1e3);
	avg( 50, 1e3);
	avg(100, 1e3);
	
	return 0;
}
