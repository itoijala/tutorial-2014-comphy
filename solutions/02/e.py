import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.optimize

pc = np.loadtxt('pc.csv')

fig = plt.figure()
ax1 = fig.add_subplot(2, 1, 1)
ax2 = fig.add_subplot(2, 1, 2)

x = np.linspace(0, 1, 1000)

for L in [10, 50, 100]:
    p, _, M = np.loadtxt("c-{}.csv".format(L), unpack=True)
    ax1.plot(p, M, "x", label="$L = {}$".format(L))
    β, ν = 5 / 36, 4 / 3
    ax2.plot((p - pc) * L**(1 / ν), M * L**(β / ν), "x", label="$L = {}$".format(L))

ax1.set_xlabel("$p$")
ax1.set_ylabel(r"$M_\infty$")
ax2.set_xlabel(r"$(p - p_{\text{c}}) L^{1 / \nu}$")
ax2.set_ylabel(r"$M_\infty L^{\beta / \nu}$")
ax1.legend(loc='upper left')
ax2.legend(loc='upper right')
fig.savefig("e.pdf")
