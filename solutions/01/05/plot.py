import numpy as np
from scipy.optimize import curve_fit

import matplotlib as mpl
import matplotlib.pyplot as plt

N, R = np.loadtxt('data', unpack=True)

def f(x, a, b):
    return a * x + b

popt, pcov = curve_fit(f, np.log(N), np.log(R))
a, b = popt
δa, δb = np.sqrt(np.diag(pcov))
print(a, δa)
print(b, δb)

fig, ax = plt.subplots(1, 1)
ax.plot(N, R, 'rx')
x = np.linspace(np.min(N), np.max(N))
ax.plot(x, np.exp(f(np.log(x), a, b)), 'b-')
ax.set_xscale('log')
ax.set_yscale('log')
fig.savefig('plot.pdf')
