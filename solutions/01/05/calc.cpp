#include <cstdio>
#include <random>
#include <tuple>

#include <Eigen/SparseCore>

template<class URNG>
std::tuple<double, double> walk(int N, URNG& rng)
{
	Eigen::SparseMatrix<int> grid(2 * N + 1, 2 * N + 1);
	int x, y;
	x = y = N;
	grid.coeffRef(x, y) = 1;
	double weight = 1;

	for (int i = 0; i < N; i++)
	{
		int x_moves[] = {+1, -1,  0,  0};
		int y_moves[] = { 0,  0, +1, -1};
		int possible_moves[4] = {};
		int possible = 0;

		for (int m = 0; m < 4; m++)
		{
			if (grid.coeff(x + x_moves[m], y + y_moves[m]) == 0)
			{
				possible_moves[possible] = m;
				possible++;
			}
		}

		if (possible == 0)
		{
			return std::make_tuple(0, 0);
		}

		std::uniform_int_distribution<int> dist(0, possible - 1);
		int move = dist(rng);
		x += x_moves[possible_moves[move]];
		y += y_moves[possible_moves[move]];
		grid.coeffRef(x, y) = 1;
		weight *= possible;
	}

	return std::make_tuple((x - N) * (x - N) + (y - N) * (y - N), weight);
}

int main()
{
	Eigen::initParallel();

	int N_min = 1;
	int N_max = 700;
	int N_step = 20;
	int walks = 10000;

	double* R = new double[N_max];

	#pragma omp parallel for schedule(dynamic)
	for (int N = N_min; N <= N_max; N += N_step)
	{
		std::mt19937 rng(N);

		double R2 = 0;
		double weight = 0;

		for (int i = 0; i < walks; i++)
		{
			double tR2;
			double tw;
			std::tie(tR2, tw) = walk(N, rng);

			if (tw == 0)
			{
				i--;
				continue;
			}

			R2 += tw * tR2;
			weight += tw;
		}

		R[N] = (R2 / weight);
		printf("%d\n", N);
	}

	auto file = fopen("data", "w");
	for (int N = N_min; N <= N_max; N += N_step)
	{
		fprintf(file, "%d %.16f\n", N, R[N]);
	}
	fclose(file);
	return 0;
}
