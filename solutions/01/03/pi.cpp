#include <cstdio>
#include <functional>
#include <random>

#include <omp.h>

#include <Eigen/Dense>

template<int n>
double volume(std::function<bool(Eigen::Matrix<double, n, 1>)> f, const Eigen::Matrix<double, n, 1>& limit1, const Eigen::Matrix<double, n, 1>& limit2, int throws)
{
	int hits = 0;

	#pragma omp parallel
	{
		std::mt19937 gen(omp_get_thread_num());
		std::uniform_real_distribution<double> dists[n];
		for (int i = 0; i < n; i++)
		{
			dists[i] = std::uniform_real_distribution<>(limit1[i], limit2[i]);
		}

		#pragma omp for reduction(+:hits)
		for (int t = 0; t < throws; t++)
		{
			Eigen::Matrix<double, n, 1> point = Eigen::Matrix<double, n, 1>::LinSpaced(0, n - 1).unaryExpr([&gen, &dists](double x) { return dists[int(x)](gen); });
			if (f(point))
			{
				hits++;
			}
		}
	}

	double V = (limit2 - limit1).prod();
	return V * hits / throws;
}

int main()
{
	Eigen::initParallel();

	Eigen::Vector2d limit1;
	limit1 << -1, -1;
	Eigen::Vector2d limit2;
	limit2 << 1, 1;
	printf("%.16f", volume<2>([](Eigen::Vector2d p) { return p.squaredNorm() <= 1; }, limit1, limit2, 100000000));

	return 0;
}
