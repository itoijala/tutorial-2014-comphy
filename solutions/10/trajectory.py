import sys

import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

t, x, y, vx, vy = np.loadtxt('trajectory-{}.dat'.format(sys.argv[1]), unpack=True)

fig, ax = plt.subplots(1, 1, subplot_kw={'projection': '3d'})

ax.plot(x, y, vy, '-')

ax.set_xlabel('$x$')
ax.set_ylabel('$y$')
ax.set_zlabel('$v_y$')

fig.savefig('trajectory-{}.pdf'.format(sys.argv[1]))
