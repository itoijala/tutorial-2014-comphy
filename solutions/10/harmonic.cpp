#include <cstdio>

#include "rungeKutta.h"
#include "symplectic.h"

typedef std::function<std::tuple<std::vector<double>,
                                 std::vector<Eigen::VectorXd>,
                                 std::vector<Eigen::VectorXd>>(
    std::function<Eigen::VectorXd(Eigen::VectorXd v)> dTdv,
    std::function<Eigen::VectorXd(Eigen::VectorXd r)> dVdr, const Eigen::VectorXd& r0,
    Eigen::VectorXd v0, size_t N, double t0, double T)> Integrator;

void harmonicOscillator(Integrator integrator, std::string name)
{
	const size_t N = 1000;
	const double t0 = 0;
	const double T = 200;
	const Eigen::VectorXd x0 = Eigen::VectorXd::Zero(1);
	const Eigen::VectorXd v0 = Eigen::VectorXd::Ones(1);

	std::vector<double> t;
	std::vector<Eigen::VectorXd> x;
	std::vector<Eigen::VectorXd> v;

	std::tie(t, x, v) = integrator([](Eigen::VectorXd v){ return v; },
	                               [](Eigen::VectorXd x){ return x; },
	                               x0, v0,
	                               N, t0, T);

	auto file = std::fopen((std::string("harmonic-") + name + ".dat").c_str(), "w");
	for (size_t i = 0; i <= N; i++)
	{
		std::fprintf(file, "%.20f %.20f %.20f\n", t[i], x[i][0], v[i][0]);
	}
	std::fclose(file);
}

int main()
{
	std::vector<Integrator> integrators{forwardEuler, symplecticEuler, verlet, rungeKutta4, symplectic4};
	std::vector<std::string> names{"forwardEuler", "symplecticEuler", "verlet", "rungeKutta4", "symplectic4"};
	for (size_t i = 0; i < integrators.size(); i++)
	{
		harmonicOscillator(integrators[i], names[i]);
	}
	return 0;
}
