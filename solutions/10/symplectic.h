#include <functional>
#include <tuple>
#include <vector>

#include <Eigen/Dense>

#ifndef SYMPLECTIC_H
#define SYMPLECTIC_H

std::tuple<std::vector<double>, std::vector<Eigen::VectorXd>,
           std::vector<Eigen::VectorXd>>
symplectic(std::function<Eigen::VectorXd(Eigen::VectorXd v)> dTdv,
           std::function<Eigen::VectorXd(Eigen::VectorXd r)> dVdr,
           Eigen::VectorXd r0, Eigen::VectorXd v0, size_t N, double t0, double T,
           Eigen::VectorXd c, Eigen::VectorXd d);

std::tuple<std::vector<double>, std::vector<Eigen::VectorXd>,
           std::vector<Eigen::VectorXd>>
symplecticEuler(std::function<Eigen::VectorXd(Eigen::VectorXd v)> dTdv,
                std::function<Eigen::VectorXd(Eigen::VectorXd r)> dVdr,
                Eigen::VectorXd r0, Eigen::VectorXd v0, size_t N, double t0, double T);

std::tuple<std::vector<double>, std::vector<Eigen::VectorXd>,
           std::vector<Eigen::VectorXd>>
verlet(std::function<Eigen::VectorXd(Eigen::VectorXd v)> dTdv,
       std::function<Eigen::VectorXd(Eigen::VectorXd r)> dVdr,
       Eigen::VectorXd r0, Eigen::VectorXd v0, size_t N, double t0, double T);

std::tuple<std::vector<double>, std::vector<Eigen::VectorXd>,
           std::vector<Eigen::VectorXd>>
symplectic4(std::function<Eigen::VectorXd(Eigen::VectorXd v)> dTdv,
            std::function<Eigen::VectorXd(Eigen::VectorXd r)> dVdr,
            Eigen::VectorXd r0, Eigen::VectorXd v0, size_t N, double t0, double T);

#endif
