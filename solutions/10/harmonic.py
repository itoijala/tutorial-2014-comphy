import sys

import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt

fig, (axx, axv, axE) = plt.subplots(nrows=3, sharex=True)
fig.set_figheight(10)

for name in sys.argv[1:]:
    t, x, v = np.loadtxt('harmonic-' + name + '.dat', unpack=True)

    axx.plot(t, x, '-', label=name)
    axv.plot(t, v, '-', label=name)
    axE.plot(t, 0.5 * (x**2 + v**2), '-', label=name)

axx.plot(t, np.sin(t), '--', label='exact')
axv.plot(t, np.cos(t), '--', label='exact')
axE.axhline(y=0.5, ls='--', label='exact')

axx.set_ylim(-2, 2)
axv.set_ylim(-2, 2)
axE.set_ylim(0.4, 1.4)

axE.set_xlabel('$t$')
axx.set_ylabel('$x$')
axv.set_ylabel('$v$')
axE.set_ylabel('$E$')

axx.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=2, mode="expand", borderaxespad=0.)
fig.savefig('harmonic.pdf')
