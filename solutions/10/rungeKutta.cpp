#include "rungeKutta.h"

#include <tuple>
#include <vector>

#include <Eigen/Dense>

std::tuple<std::vector<double>, std::vector<Eigen::VectorXd>>
rungeKutta(std::function<Eigen::VectorXd(double t, Eigen::VectorXd x)> f,
           Eigen::VectorXd x0, size_t N, double t0, double T,
           Eigen::MatrixXd a, Eigen::VectorXd b, Eigen::VectorXd c)
{
	const double h = (T - t0) / N;

	std::vector<double> t(N + 1);
	std::vector<Eigen::VectorXd> x(N + 1, Eigen::VectorXd(x0.size()));

	t[0] = t0;
	x[0] = x0;

	Eigen::MatrixXd k(x0.size(), c.size());
	for(int i = 1; i <= N; i++)
	{
		for(int j = 0; j < k.cols(); j++)
		{
			k.col(j) = h * f(t[i - 1] + h * c[j], x[i - 1] + (k * a.row(j).asDiagonal()).rowwise().sum());
		}
		x[i] = x[i - 1] + (k * b.asDiagonal()).rowwise().sum();
		t[i] = t[i - 1] + h;
	}

	return std::make_tuple(t, x);
}

std::tuple<std::vector<double>, std::vector<Eigen::VectorXd>,
           std::vector<Eigen::VectorXd>>
rungeKutta(std::function<Eigen::VectorXd(Eigen::VectorXd v)> dTdv,
           std::function<Eigen::VectorXd(Eigen::VectorXd r)> dVdr,
           Eigen::VectorXd r0, Eigen::VectorXd v0, size_t N, double t0, double T,
           Eigen::MatrixXd a, Eigen::VectorXd b, Eigen::VectorXd c)
{
	const size_t size = r0.size();

	Eigen::VectorXd x0(2 * size);
	x0 << r0,
	      v0;

	std::vector<double> t;
	std::vector<Eigen::VectorXd> x;
	std::tie(t, x) = rungeKutta([dTdv, dVdr, size](double t, Eigen::VectorXd x)
	{
		Eigen::VectorXd y(2 * size);
		y <<  dTdv(x.tail(size)),
		     -dVdr(x.head(size));
		return y;
	}, x0, N, t0, T, a, b, c);

	std::vector<Eigen::VectorXd> r(N + 1, Eigen::VectorXd(size));
	std::vector<Eigen::VectorXd> v(N + 1, Eigen::VectorXd(size));
	for (int i = 0; i <= N; i++)
	{
		r[i] = x[i].head(size);
		v[i] = x[i].tail(size);
	}

	return std::make_tuple(t, r, v);
}

std::tuple<std::vector<double>, std::vector<Eigen::VectorXd>,
           std::vector<Eigen::VectorXd>>
forwardEuler(std::function<Eigen::VectorXd(Eigen::VectorXd v)> dTdv,
             std::function<Eigen::VectorXd(Eigen::VectorXd r)> dVdr,
             Eigen::VectorXd r0, Eigen::VectorXd v0, size_t N, double t0, double T)
{
	Eigen::MatrixXd a = Eigen::MatrixXd::Zero(1, 1);
	Eigen::VectorXd b = Eigen::VectorXd::Ones(1);
	Eigen::VectorXd c = Eigen::VectorXd::Zero(1);

	return rungeKutta(dTdv, dVdr, r0, v0, N, t0, T, a, b, c);
}

std::tuple<std::vector<double>, std::vector<Eigen::VectorXd>,
           std::vector<Eigen::VectorXd>>
rungeKutta4(std::function<Eigen::VectorXd(Eigen::VectorXd v)> dTdv,
            std::function<Eigen::VectorXd(Eigen::VectorXd r)> dVdr,
            Eigen::VectorXd r0, Eigen::VectorXd v0, size_t N, double t0, double T)
{
	Eigen::MatrixXd a(4, 4);
	a << 0  , 0  , 0, 0,
	     0.5, 0  , 0, 0,
	     0  , 0.5, 0, 0,
	     0  , 0  , 1, 0;
	Eigen::VectorXd b(4);
	b << 1. / 6.,
	     1. / 3.,
	     1. / 3.,
	     1. / 6.;
	Eigen::VectorXd c(4);
	c << 0,
	     1. / 2.,
	     1. / 2.,
	     1;

	return rungeKutta(dTdv, dVdr, r0, v0, N, t0, T, a, b, c);
}
