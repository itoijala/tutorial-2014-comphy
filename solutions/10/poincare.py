import sys

import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt

t, y, vy = np.loadtxt('poincare-{}.dat'.format(sys.argv[1]), unpack=True)

fig, ax = plt.subplots(1, 1)

ax.plot(y, vy, ',')

ax.set_xlabel('$y$')
ax.set_ylabel('$v_y$')

fig.savefig('poincare-{}.pdf'.format(sys.argv[1]))
