#include "symplectic.h"

#include <cmath>
#include <tuple>
#include <vector>

#include <Eigen/Dense>

std::tuple<std::vector<double>, std::vector<Eigen::VectorXd>, std::vector<Eigen::VectorXd>>
symplectic(std::function<Eigen::VectorXd(Eigen::VectorXd v)> dTdv,
           std::function<Eigen::VectorXd(Eigen::VectorXd r)> dVdr,
           Eigen::VectorXd r0, Eigen::VectorXd v0, size_t N, double t0, double T,
           Eigen::VectorXd c, Eigen::VectorXd d)
{
	const double h = (T - t0) / N;

	std::vector<double> t(N + 1);
	std::vector<Eigen::VectorXd> r(N + 1, Eigen::VectorXd(r0.size()));
	std::vector<Eigen::VectorXd> v(N + 1, Eigen::VectorXd(r0.size()));

	t[0] = t0;
	r[0] = r0;
	v[0] = v0;

	for (int i = 1; i <= N; i++)
	{
		r[i] = r[i - 1];
		v[i] = v[i - 1];
		for (int j = c.size() - 1; j >= 0; j--)
		{
			v[i] -= d[j] * h * dVdr(r[i]);
			r[i] += c[j] * h * dTdv(v[i]);
		}
		t[i] = t[i - 1] + h;
	}
	return std::make_tuple(t, r, v);
}

std::tuple<std::vector<double>, std::vector<Eigen::VectorXd>,
           std::vector<Eigen::VectorXd>>
symplecticEuler(std::function<Eigen::VectorXd(Eigen::VectorXd v)> dTdv,
                std::function<Eigen::VectorXd(Eigen::VectorXd r)> dVdr,
                Eigen::VectorXd r0, Eigen::VectorXd v0, size_t N, double t0, double T)
{
	Eigen::VectorXd c = Eigen::VectorXd::Ones(1);
	Eigen::VectorXd d = Eigen::VectorXd::Ones(1);

	return symplectic(dTdv, dVdr, r0, v0, N, t0, T, c, d);
}

std::tuple<std::vector<double>, std::vector<Eigen::VectorXd>,
           std::vector<Eigen::VectorXd>>
verlet(std::function<Eigen::VectorXd(Eigen::VectorXd v)> dTdv,
       std::function<Eigen::VectorXd(Eigen::VectorXd r)> dVdr,
       Eigen::VectorXd r0, Eigen::VectorXd v0, size_t N, double t0, double T)
{
	Eigen::VectorXd c = Eigen::VectorXd::Constant(2, 0.5);
	Eigen::VectorXd d(2);
	d << 1,
	     0;

	return symplectic(dTdv, dVdr, r0, v0, N, t0, T, c, d);
}

std::tuple<std::vector<double>, std::vector<Eigen::VectorXd>,
           std::vector<Eigen::VectorXd>>
symplectic4(std::function<Eigen::VectorXd(Eigen::VectorXd v)> dTdv,
            std::function<Eigen::VectorXd(Eigen::VectorXd r)> dVdr,
            Eigen::VectorXd r0, Eigen::VectorXd v0, size_t N, double t0, double T)
{
	constexpr double cr2 = cbrt(2);
	constexpr double de = 2 * (2 - cr2);

	Eigen::VectorXd c(4);
	c << 1,
	     1 - cr2,
	     1 - cr2,
         1;
	c /= de;

	Eigen::VectorXd d(4);
	d << 2,
	     - 2 * cr2,
	     2,
	     0;
	d /= de;

	return symplectic(dTdv, dVdr, r0, v0, N, t0, T, c, d);
}
