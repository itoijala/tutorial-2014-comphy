#define _USE_MATH_DEFINES
#include <cmath>
#include <cstdio>
#include <functional>
#include <random>
#include <vector>

#include <omp.h>

#include <Eigen/Dense>

using namespace std;
using Eigen::VectorXd;

typedef function<VectorXd(const VectorXd&)> Force;
typedef function<double(double)> Potential;

VectorXd difference(double L, const VectorXd& r1, const VectorXd& r2)
{
	return (r2 - r1).unaryExpr([L](double x)
	{
		return x - L * floor(x / L + 0.5);
	});
}

vector<VectorXd> forces(Force f, double L, double C, const vector<VectorXd>& r)
{
	const size_t N = r.size();
	const VectorXd::Index D = r[0].size();

	vector<VectorXd> F(N, VectorXd::Zero(D));
	for (size_t i = 0; i < N; i++)
	{
		for (size_t j = 0; j < i; j++)
		{
			VectorXd diff = difference(L, r[i], r[j]);
			if (diff.norm() > C)
			{
				continue;
			}
			VectorXd force = f(diff);
			F[i] -= force;
			F[j] += force;
		}
	}
	return F;
}

tuple<vector<VectorXd>, vector<VectorXd>, vector<VectorXd>> MDsimulation(Force f, double h, double L, double C, const vector<VectorXd>& r0, const vector<VectorXd>& v0, const vector<VectorXd>& F0)
{
	const size_t N = r0.size();
	const VectorXd::Index D = r0[0].size();

	vector<VectorXd> r(N);
	for (size_t i = 0; i < N; i++)
	{
		r[i] = (r0[i] + h * v0[i] + 0.5 * h * h * F0[i]).unaryExpr([L](double x)
		{
			return x - L * floor(x / L);
		});
	}

	vector<VectorXd> F = forces(f, L, C, r);
	vector<VectorXd> v(N);
	for (size_t i = 0; i < N; i++)
	{
		v[i] = v0[i] + 0.5 * h * (F[i] + F0[i]);
	}

	return make_tuple(r, v, F);
}

VectorXd calc_v_cms(const vector<VectorXd>& v)
{
	const size_t N = v.size();
	const VectorXd::Index D = v[0].size();

	VectorXd sum = VectorXd::Zero(D);
	for (size_t i = 0; i < N; i++)
	{
		sum += v[i] / N;
	}
	return sum;
}

double calc_E_pot(Potential V, double L, double C, const vector<VectorXd>& r)
{
	const size_t N = r.size();
	const VectorXd::Index D = r[0].size();

	double sum = 0;
	for (size_t i = 0; i < N; i++)
	{
		for (size_t j = 0; j < i; j++)
		{
			VectorXd diff = difference(L, r[i], r[j]);
			double d = diff.norm();
			if (d > C)
			{
				continue;
			}
			sum += V(d);
		}
	}
	return sum;
}

double calc_E_kin(const vector<VectorXd>& v)
{
	const size_t N = v.size();
	const VectorXd::Index D = v[0].size();

	double sum = 0;
	for (size_t i = 0; i < N; i++)
	{
		sum += v[i].squaredNorm();
	}
	return 0.5 * sum;
}

VectorXd calc_g(double L, size_t bins, const vector<VectorXd>& r)
{
	const size_t N = r.size();
	const VectorXd::Index D = r[0].size();

	VectorXd g = VectorXd::Zero(bins);
	for (size_t i = 0; i < N; i++)
	{
		for (size_t j = 0; j < i; j++)
		{
			VectorXd diff = difference(L, r[i], r[j]);
			double d = diff.norm();
			if (d < L)
			{
				g[size_t(floor(bins * d / L))]++;
			}
		}
	}
	return g;
}

void work(double L, size_t sqrtN, double T)
{
	const size_t D = 2;
	const size_t N = sqrtN * sqrtN;
	const size_t time = 1e6;
	const size_t time_equi = 1e4;
	const double C = 3;
	const size_t bins = 1000;
	const double h = 0.01;

	Force Lennard_Jones_force = [](const VectorXd &diff)
	{
		double r = diff.norm();
		return 24 * (2 * pow(1 / r, 14) - pow(1 / r, 8)) * diff;
	};

	Potential Lennard_Jones_potential = [](double r)
	{
		return 4 * (pow(1 / r, 12) - pow(1 / r, 6));
	};

	vector<VectorXd> r0(N);
	for (size_t m = 0; m < sqrtN; m++)
	{
		for (size_t n = 0; n < sqrtN; n++)
		{
			r0[sqrtN * m + n] = VectorXd(D);
			r0[sqrtN * m + n] << L / (2 * sqrtN) * (1 + 2 * m),
			                     L / (2 * sqrtN) * (1 + 2 * n);
		}
	}

	mt19937 generator;
	normal_distribution<double> distribution(-1, 1);

	vector<VectorXd> v0(N);
	for (size_t i = 0; i < N; i++)
	{
		v0[i] = VectorXd(D);
		v0[i] << distribution(generator),
		         distribution(generator);
	}

	VectorXd vg = calc_v_cms(v0);
	for (size_t i = 0; i < N; i++)
	{
		v0[i] -= vg;
	}

	double E_kin0 = calc_E_kin(v0);
	for (size_t i = 0; i < N; i++)
	{
		v0[i] *= sqrt(0.5 * T / E_kin0 * D * (N - 1));
	}

	vector<VectorXd> F0 = forces(Lennard_Jones_force, L, C, r0);

	vector<double> v_cms(time);
	vector<double> E_kin(time);
	vector<double> E_pot(time);
	VectorXd g = VectorXd::Zero(bins);

	v_cms[0] = calc_v_cms(v0).norm();
	E_kin[0] = calc_E_kin(v0);
	E_pot[0] = calc_E_pot(Lennard_Jones_potential, L, C, r0);

	for (size_t t = 1; t < time; t++)
	{
		vector<VectorXd> r, v, F;
		tie(r, v, F) = MDsimulation(Lennard_Jones_force, h, L, C, r0, v0, F0);
		r0 = r;
		v0 = v;
		F0 = F;

		v_cms[t] = calc_v_cms(v).norm();
		E_kin[t] = calc_E_kin(v);
		E_pot[t] = calc_E_pot(Lennard_Jones_potential, L, C, r);

		if (t >= time_equi)
		{
			g += (calc_g(L, bins, r) - g) / (t - time_equi + 1);
		}
	}

	char name[256];
	sprintf(name, "meas-%.0f-%zu-%.2f.csv", L, N, T);
	auto file = fopen(name, "w");
	for (size_t t = 0; t < time; t++)
	{
		fprintf(file, "%zu %.15f %.15f %.15f\n", t, v_cms[t], E_kin[t], E_pot[t]);
	}
	fclose(file);

	sprintf(name, "g-%.0f-%zu-%.2f.csv", L, N, T);
	file = fopen(name, "w");
	for (size_t b = 0; b < bins; b++)
	{
		g[b] *= pow(L, D) / (N * N * (pow(double(b) / bins * L + L / bins, D) - pow(double(b) / bins * L, D)) * pow(M_PI, D / 2.)) * tgamma(D / 2. + 1);
		fprintf(file, "%zu %.15f\n", b, g[b]);
	}
	fclose(file);
}

int main()
{
	Eigen::initParallel();

	vector<double> L{8, 8,    6,    4,    16,   12,   8};
	vector<double> N{4, 4,    4,    4,    8,    8,    8};
	vector<double> T{3, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01};

	#pragma omp parallel for schedule(dynamic)
	for (size_t i = 0; i < L.size(); i++)
	{
		work(L[i], N[i], T[i]);
	}
}
