#include <bitset>
#include <fstream>
#include <functional>
#include <iostream>
#include <random>
#include <tuple>
#include <vector>

#include <Eigen/Dense>

unsigned int swap(unsigned int num, unsigned int i, unsigned int j)
{
	unsigned int x = ((num >> i) ^ (num >> j)) & 1;
	return num ^ ((x << i) | (x << j));
}

std::tuple<Eigen::MatrixXd, std::vector<int>> genHeisenberg(double J, int spins)
{
	const int size = pow(2, spins);

	std::vector<int> sizes(spins + 1, 1);
	std::vector<std::vector<int>> spin_states(spins + 1);
	for (int i = 1, s = 1; i <= spins; i++)
	{
		spin_states[i].reserve(s);
		sizes[i] = s = (s * (spins + 1 - i)) / i;
	}

	for (int i = 0; i < size; i++)
	{
		const int bits = std::bitset<sizeof(int) * 8>(i).count();
		spin_states[bits].push_back(i);
	}

	std::vector<int> states(size);
	std::vector<int> anti(size);
	for (int i = 0, index = 0; i <= spins; i++)
	{
		for (int j = 0; j < spin_states[i].size(); j++)
		{
			const int state = spin_states[i][j];
			states[index] = state;
			anti[state] = index;
			index++;
		}
	}

	Eigen::MatrixXd hamiltonian = Eigen::MatrixXd::Zero(size, size);
	for (int i = 0; i < size; i++)
	{
		for (int cur = 0; cur < spins; cur++)
		{
			int next = (cur + 1) % spins;
			hamiltonian(i, anti[swap(states[i], cur, next)]) += 2;
			hamiltonian(i, i) += -1;
		}
	}
	hamiltonian *= -J / 4;

	return std::make_tuple(hamiltonian, sizes);
}

std::tuple<std::tuple<double, double>, std::tuple<Eigen::VectorXd, Eigen::VectorXd>> evConvergence(const Eigen::VectorXd &alpha, const Eigen::VectorXd &beta)
{
	Eigen::MatrixXd mat = alpha.asDiagonal();
	mat.diagonal(+1) = beta.tail(beta.size() - 1);
	mat.diagonal(-1) = beta.tail(beta.size() - 1);

	Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> solver(mat);
	return std::make_tuple(std::make_tuple(solver.eigenvalues()[0], solver.eigenvalues()[mat.cols() - 1]), std::make_tuple(solver.eigenvectors().col(0), solver.eigenvectors().col(mat.cols() - 1)));
}

std::tuple<std::vector<std::tuple<double, double>>, std::tuple<Eigen::VectorXd, Eigen::VectorXd>> lanczos(const Eigen::MatrixXd &mat)
{
	int dim = mat.rows();
	if (dim == 1)
	{
		Eigen::VectorXd id(1);
		id << 1;
		return std::make_tuple(std::vector<std::tuple<double, double>>{std::make_tuple(mat(0, 0), mat(0, 0))}, std::make_tuple(id, id));
	}

	std::mt19937 gen;
	std::uniform_real_distribution<double> dist(-1, 1);
	std::function<double()> ran = std::bind(dist, gen);

	std::vector<std::tuple<double, double>> evs(dim);
	
	Eigen::VectorXd alpha(dim);
	Eigen::VectorXd beta(dim);
	std::vector<Eigen::VectorXd> evec(dim + 1);
	Eigen::MatrixXd V(dim, dim);

	evec[0] =  Eigen::VectorXd::Zero(dim);
	evec[1] = V.col(0) = Eigen::VectorXd::Zero(dim).unaryExpr([&ran](double x){ return ran(); }).normalized();
	beta[0] = 0;

	for (int i = 1; i < dim; i++)
	{
		auto tmp = mat * evec[i];
		alpha[i - 1] = evec[i].adjoint() * tmp;
		auto tmp2 = tmp - alpha[i - 1] * evec[i] - beta[i - 1] * evec[i - 1];
		beta[i] = tmp2.norm();
		evec[i + 1] = V.col(i) = tmp2 / beta[i];
		std::tie(evs[i - 1], std::ignore) = evConvergence(alpha, beta);
	}
	alpha[dim - 1] = evec[dim].adjoint() * mat * evec[dim];

	std::tuple<Eigen::VectorXd, Eigen::VectorXd> tmp;
	Eigen::VectorXd tmp2, tmp3;
	std::tie(evs[dim - 1], tmp) = evConvergence(alpha, beta);
	std::tie(tmp2, tmp3) = tmp;
	tmp2 = V * tmp2;
	tmp3 = V * tmp3;

	return std::make_tuple(evs, std::make_tuple(tmp2, tmp3));
}

int main()
{
	const double J = 1.0;
	const int spins = 10;

	Eigen::MatrixXd hamiltonian;
	std::vector<int> sizes;
	std::tie(hamiltonian, sizes) = genHeisenberg(J, spins);

	for (int i = 0, index = 0; i <= spins; i++)
	{
		int s = sizes[i];
		std::vector<std::tuple<double, double>> evals;
		std::tie(evals, std::ignore) = lanczos(hamiltonian.block(index, index, s, s));
		Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> solver(hamiltonian.block(index, index, s, s), false);
		double emin = solver.eigenvalues()[0];
		double emax = solver.eigenvalues()[s - 1];
		index += s;

		char name[256];
		sprintf(name, "%i.dat", i);
		auto file = fopen(name, "w");
		fprintf(file, "%.20f %.20f\n", emin, emax);
		
		for (int j = 0; j < s; j++)
		{
			double min, max;
			std::tie(min, max) = evals[j];
			fprintf(file, "%i %.20f %.20f\n", j, min, max);
		}
		fclose(file);
	}
}
