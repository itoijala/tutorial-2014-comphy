import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pgf import FigureCanvasPgf
mpl.backend_bases.register_backend('pdf', FigureCanvasPgf)

for i in range(0, 11):
    emin, emax = np.loadtxt('{}.dat'.format(i))[0]
    it, lmin, lmax = np.loadtxt('{}.dat'.format(i), unpack=True, skiprows=1)
    fig, ax = plt.subplots(1, 1)
    ax.plot(it, lmax, 'x', label='Lanczos Max')
    ax.plot(it, lmin, 'x', label='Lanczos Min')
    ax.axhline(y=emax, label='Eigen Max')
    ax.axhline(y=emin, label='Eigen Min')
    ax.legend(loc='best')
    fig.savefig('{}.pdf'.format(i))
