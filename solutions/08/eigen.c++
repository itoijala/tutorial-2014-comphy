
#include <iostream>
#include <fstream>

#include <Eigen/Eigen>
#include <Eigen/Eigenvalues>

using namespace Eigen;

/**
 * Swap bits i and j in num
 */
inline int swap(int num, int i, int j) {
    unsigned int x = ((num >> i) ^ (num >> j)) & 1;
    return num ^ ((x << i) | (x << j));
}

int main(int argc, char *argv[])
{
    const double J = 1.0;
    const int spins = 13;

    std::cout << "Initializing Hamiltonian..." << std::endl;

    int size = 1 << spins;
    MatrixXd hamiltonian = MatrixXd::Zero(size, size);

    // loop over all possible inputs of H
    for (int i = 0; i < size; i++) {
        // sum over spins
        for (int cur = 0; cur < spins; cur++) {
            int next = (cur + 1) % spins;

            hamiltonian(i, swap(i, cur, next)) += 2;
            hamiltonian(i, i) += -1;
        }
    }

    hamiltonian *= -J/4;

    std::cout << "Diagonalizing..." << std::endl;

    SelfAdjointEigenSolver<MatrixXd> solver(hamiltonian, false);

    std::ofstream out;
    out.open("energies.txt");
    out << solver.eigenvalues() << std::endl;

    return 0;
}


