#include <bitset>
#include <fstream>
#include <iostream>
#include <vector>

#include <Eigen/Eigen>
#include <Eigen/Eigenvalues>

using std::vector;

/*
 * Swap bits i and j in num
 */
inline int swap(int num, int i, int j)
{
    unsigned int x = ((num >> i) ^ (num >> j)) & 1;
    return num ^ ((x << i) | (x << j));
}

int main()
{
    const double J = 1.0;
    const int spins = 13;
    const int size = 1 << spins;

    vector<vector<int>> spin_states(spins + 1);
    vector<int> sizes(spins + 1, 1);
    for (int i = 1, s = 1; i <= spins; i++)
    {
        spin_states[i].reserve(s);
        sizes[i] = s = (s * (spins + 1 - i)) / i;
    }

    for (int i = 0; i < size; i++)
    {
        int bits = std::bitset<spins>(i).count();
        spin_states[bits].push_back(i);
    }

    vector<int> states(size);
    vector<int> anti(size);
    for (int i = 0, index = 0; i <= spins; i++)
    {
        for (int j = 0; j < spin_states[i].size(); j++)
        {
            int state = spin_states[i][j];
            states[index] = state;
            anti[state] = index;
            index++;
        }
    }

    Eigen::MatrixXd hamiltonian = Eigen::MatrixXd::Zero(size, size);
    for (int i = 0; i < size; i++) {
        for (int cur = 0; cur < spins; cur++) {
            int next = (cur + 1) % spins;
            hamiltonian(i, anti[swap(states[i], cur, next)]) += 2;
            hamiltonian(i, i) += -1;
        }
    }
    hamiltonian *= -J/4;

    std::ofstream out("energies.txt");
    for (int i = 0, index = 0; i <= spins; i++)
    {
        int s = sizes[i];
        Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> solver(hamiltonian.block(index, index, s, s), false);
        out << solver.eigenvalues() << std::endl;
        index += s;
    }
    out.close();
}
