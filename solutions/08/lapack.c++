
#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>

extern "C" {
    void dsyev_(char*    jobz,
                char*    uplo,
                int*     n,
                double*  a,
                int*     lda,
                double*  w,
                double*  work,
                int*     lwork,
                int*     info);
}

/**
 * Swap bits i and j in num
 */
inline int swap(int num, int i, int j) {
    unsigned int x = ((num >> i) ^ (num >> j)) & 1;
    return num ^ ((x << i) | (x << j));
}

int main(int argc, char *argv[])
{
    const double J = 1.0;
    const int spins = 13;

    std::cout << "Initializing Hamiltonian..." << std::endl;

    int size = 1 << spins;
    double *hamiltonian = new double[size*size];
    for (int i=0; i<size; i++) {
        for(int j=0; j<size; j++) {
            hamiltonian[i + j * size] = 0;
        }
    }

    // loop over all possible inputs of H
    for (int i = 0; i < size; i++) {
        // sum over spins
        for (int cur = 0; cur < spins; cur++) {
            int next = (cur + 1) % spins;

            // use column-major order
            hamiltonian[i + size * swap(i, cur, next)] -= J / 2;
            hamiltonian[i + size * i] += J / 4;
        }
    }

    std::cout << "Diagonalizing..." << std::endl;

    char jobz = 'N';
    char uplo = 'U';
    double wwork = 0;
    int lwork = -1;
    int info = 0;
    std::vector<double> eigenvalues(size, 0);

    // Get optimal size of workspace
    dsyev_(&jobz, &uplo, &size, hamiltonian, &size,
           &eigenvalues[0], &wwork, &lwork, &info);

    lwork = ceil(wwork);
    double* work = new double[lwork];

    // Perform diagonalization
    dsyev_(&jobz, &uplo, &size, hamiltonian, &size,
           &eigenvalues[0], work, &lwork, &info);

    std::ofstream out;
    out.open("energies.txt");
    for (auto w: eigenvalues) {
        out << w << std::endl;
    }
    out.close();

    return 0;
}


