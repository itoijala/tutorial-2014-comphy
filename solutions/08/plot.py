
from pylab import *

E = loadtxt('energies.txt')

@vectorize
def Z(beta):
    return sum(exp(-beta * E))

@vectorize
def U(beta):
    return sum(E * exp(-beta * E)) / Z(beta)

@vectorize
def C(beta):
    return beta**2 * (sum(E**2 * exp(-beta * E)) / Z(beta) - U(beta)**2)

beta = logspace(-1, 2, 1000)

fig = figure(figsize=(8, 8))

suptitle('Thermodynamics of the Heisenberg model (10 spins)')
subplot(311)
plot(beta, Z(beta), lw=1.5)
ax = gca()
setp(ax.get_yticklabels(), fontsize=8)
xlabel('$\\beta$')
ylabel('$Z$')
yscale('log')
xscale('log')
subplot(312)
plot(beta, U(beta), lw=1.5)
xlabel('$\\beta$')
ylabel('$U$')
xscale('log')
subplot(313)
plot(beta, C(beta), lw=1.5)
xlabel('$\\beta$')
ylabel('$C_V$')
xscale('log')

tight_layout()
subplots_adjust(top=0.92)
savefig('plot.pdf')
