#include <cmath>
#include <cstdio>
#include <functional>
#include <iostream>
#include <stack>
#include <tuple>

#include <Eigen/Dense>

using namespace std;

using Eigen::ArrayXd;
using Eigen::ArrayXXi;

double deltaE(const ArrayXXi& a, double J, int x, int y, int s, int old)
{
	int Nx = a.rows();
	int Ny = a.cols();

	double dE = 0;
	for (int i = -1; i <= 1; i +=2)
	{
		int nx = (x + i + Nx) % Nx;
		dE += (a(nx, y) == s) - (a(nx, y) == old);
		int ny = (y + i + Ny) % Ny;
		dE += (a(x, ny) == s) - (a(x, ny) == old);
	}
	return -J * dE;
}

template<class URNG>
tuple<double, double> metropolis(ArrayXXi& a, int Q, double J, double T, URNG& gen)
{
	int Nx = a.rows();
	int Ny = a.cols();
	uniform_int_distribution<> dist_x(0, Nx - 1);
	uniform_int_distribution<> dist_y(0, Ny - 1);
	uniform_int_distribution<> dist_s(0, Q - 1);
	uniform_real_distribution<> dist(0, 1);

	double dE = 0;
	double dM = 0;
	for (int n = 0; n < Nx * Ny; n++)
	{
		int x = dist_x(gen);
		int y = dist_y(gen);

		int old = a(x, y);
		int s;
		do
		{
			s = dist_s(gen);
		}
		while (s == old);

		double tE = deltaE(a, J, x, y, s, old);
		if (tE <= 0 || dist(gen) < exp(- tE / T))
		{
			a(x, y) = s;
			dE += tE;
			dM += (s == 0) - (old == 0);
		}
	}

	return make_tuple(dE / (Nx * Ny), dM / (Nx * Ny));
}

template<class URNG>
tuple<int, double, double> wolff_single(ArrayXXi& a, int Q, double J, double T, URNG& gen)
{
	int Nx = a.rows();
	int Ny = a.cols();
	int x = uniform_int_distribution<>(0, Nx - 1)(gen);
	int y = uniform_int_distribution<>(0, Ny - 1)(gen);

	int old = a(x, y);
	uniform_int_distribution<> dist_s(0, Q - 1);
	int s;
	do
	{
		s = dist_s(gen);
	}
	while (s == old);

	uniform_real_distribution<> dist(0, 1);

	a(x, y) = s;
	stack<tuple<int, int>> todo;
	todo.push(make_tuple(x, y));
	double dE = deltaE(a, J, x, y, s, old);
	int flipped = 1;
	while (!todo.empty())
	{
		int x, y;
		tie(x, y) = todo.top();
		todo.pop();

		for (int i = -1; i <= 1; i += 2)
		{
			int nx = (x + i + Nx) % Nx;
			if (a(nx, y) == old && dist(gen) > exp(- 2 * J / T))
			{
				a(nx, y) = s;
				todo.push(make_tuple(nx, y));
				dE += deltaE(a, J, nx, y, s, old);
				flipped++;
			}

			int ny = (y + i + Ny) % Ny;
			if (a(x, ny) == old && dist(gen) > exp(- 2 * J / T))
			{
				a(x, ny) = s;
				todo.push(make_tuple(x, ny));
				dE += deltaE(a, J, x, ny, s, old);
				flipped++;
			}
		}
	}

	double dM = flipped * (old == 0 ? -1 : (s == 0 ? +1 : 0));
	return make_tuple(flipped, dE / (Nx * Ny), dM / (Nx * Ny));
}

template<class URNG>
tuple<double, double> wolff(ArrayXXi& a, int Q, double J, double T, URNG& gen)
{
	int Nx = a.rows();
	int Ny = a.cols();

	double dE = 0;
	double dM = 0;

	int flipped = 0;
	while (flipped < Nx * Ny)
	{
		double f, tE, tM;
		tie(f, tE, tM) = wolff_single(a, Q, J, T, gen);
		flipped += f;
		dE += tE;
		dM += tM;
	}

	return make_tuple(dE, dM);
}

tuple<double, double, ArrayXd> measure(function<tuple<double, double>(ArrayXXi&, int, double, double, mt19937&)> algorithm, int Nx, int Ny, int Q, double J, double T, int thermalise_steps, int steps, int correlation_steps)
{
	mt19937 gen;
	uniform_int_distribution<> dist(0, Q - 1);
	ArrayXXi a = ArrayXXi::Zero(Nx, Ny).unaryExpr([&gen, &dist](int x) { return dist(gen); });

	double E = 0;
	double M = 0;
	for (int x = 0; x < Nx; x++)
	{
		for (int y = 0; y < Ny; y++)
		{
			E += a(x, y) == a((x + 1 + Nx) % Nx, y);
			E += a(x, y) == a(x, (y + 1 + Ny) % Ny);
			M += a(x, y) == 0;
		}
	}
	E *= -J / (Nx * Ny);
	M /= Nx * Ny;

	for (int n = 0; n < thermalise_steps; n++)
	{
		double dE, dM;
		tie(dE, dM) = algorithm(a, Q, J, T, gen);
		E += dE;
		M += dM;
	}

	double mE = 0;
	double mM = 0;
	ArrayXd window(correlation_steps);
	ArrayXd correlation(correlation_steps);

	for (int n = 0; n < steps; n++)
	{
		double dE, dM;
		tie(dE, dM) = algorithm(a, Q, J, T, gen);
		E += dE;
		if (dM < 0 && dM > abs(M))
		{
			cout << T << " " << M << " " << dM << endl;
		}
		M += dM;

		mE += (E - mE) / (n + 1);
		mM += (M - mM) / (n + 1);

		window.tail(correlation_steps - 1) = window.head(correlation_steps - 1).eval();
		window[0] = M;

		if (n >= correlation_steps)
		{
			correlation += (window[0] * window - correlation) / (n + 1 - correlation_steps);
		}
	}

	correlation -= mM * mM;
	return make_tuple(mE, abs((Q * mM - 1) / (Q - 1)), correlation / correlation[0]);
}

int main()
{
	vector<double> T(50);
	vector<double> E(50);
	vector<double> M(50);
	vector<ArrayXd> corr(50);
	#pragma omp parallel for
	for (int i = 0; i < 50; i++)
	{
		T[i] = 0.1 * i;
		tie(E[i], M[i], corr[i]) = measure(wolff<mt19937>, 100, 100, 2, 1, T[i], 1e3, 1e3, 1e2);
	}
	for (int i = 0; i < 50; i++)
	{
		cout << T[i] << "	" << E[i] << "	" << M[i] << endl;
		cout << corr[i] << endl;
	}
}
