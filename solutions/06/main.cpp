#include <cmath>
#include <fstream>
#include <functional>
#include <iostream>
#include <random>
#include <algorithm>

#include <omp.h>

#include <Eigen/Dense>

using namespace std;
using Eigen::ArrayXXi;

double E_spin(const ArrayXXi& a, int x, int y)
{
	int Nx = a.rows();
	int Ny = a.cols();

	double E = 0;
	E -= a(x, y) * a(x, (y + 1 + Ny) % Ny);
	E -= a(x, y) * a(x, (y - 1 + Ny) % Ny);
	E -= a(x, y) * a((x + 1 + Nx) % Nx, y);
	E -= a(x, y) * a((x - 1 + Nx) % Nx, y);
	return E;
}

template<class URNG>
tuple<double, double> mc_sweep(double T, ArrayXXi& a, URNG& rng)
{
	int Nx = a.rows();
	int Ny = a.cols();
	uniform_int_distribution<> randomNx(0, Nx - 1);
	uniform_int_distribution<> randomNy(0, Ny - 1);
	uniform_real_distribution<> randomR(0, 1);

	double dE_total = 0;
	double dM = 0;
	for (int n = 0; n < Nx * Ny; n++)
	{
		int x = randomNx(rng);
		int y = randomNy(rng);
		double dE = -2 * E_spin(a, x, y);
		if (dE <= 0 || randomR(rng) < exp(- dE / T))
		{
			a(x, y) *= -1;
			dE_total += dE;
			dM += 2 * a(x, y);
		}
	}
	return make_tuple(dE_total, dM);
}

template<class URNG>
double bootstrapping(int P, int O, vector<double> samples, URNG& gen)
{
	uniform_int_distribution<> randomCoord(0, samples.size() - 1);
	auto rand = bind(randomCoord, gen);

	double mean = 0;
	double mean2 = 0;
	for (int i = 0; i < P; i++)
	{
		double sum = 0;
		for (int j = 0; j < O; j++)
		{
			sum += samples[rand()];
		}
		sum /= O;
		mean += sum;
		mean2 += sum * sum;
	}
	return mean2 / P - pow(mean / P, 2);
}

tuple<double, double, double, double> mc(double T, const function<ArrayXXi(int, int)> generate, int Nx, int Ny, int sweeps, int equiSweeps, int O, int P)
{
	mt19937 gen;
	ArrayXXi a = generate(Nx, Ny);

	double E = 0;
	double M = 0;
	for (int x = 0; x < Nx; x++)
	{
		for (int y = 0; y < Ny; y++)
		{
			E += 0.5 * E_spin(a, x, y);
			M += a(x, y);
		}
	}

	for (int t = 0; t < equiSweeps; t++)
	{
		double dE, dM;
		tie(dE, dM)= mc_sweep(T, a, gen);
		E += dE;
		M += dM;
	}

	double E_mean = 0;
	double E2_mean = 0;
	double M_mean = 0;
	double M2_mean = 0;
	double M4_mean = 0;
	vector<double> Ms(sweeps);
	for (int t = 0; t < sweeps; t++)
	{
		double dE, dM;
		tie(dE, dM) = mc_sweep(T, a, gen);
		E += dE;
		M += dM;

		E_mean += (E - E_mean) / (t + 1);
		E2_mean += (E * E - E2_mean) / (t + 1);
		M_mean += (M - M_mean) / (t + 1);
		M2_mean += (M * M - M2_mean) / (t + 1);
		M4_mean += (M * M * M * M - M4_mean) / (t + 1);
		Ms[t] = M / (Nx * Ny);
	}
	return make_tuple((E2_mean - E_mean * E_mean) / (T * T) / (Nx * Ny), M_mean / (Nx * Ny), bootstrapping(P, O, Ms, gen), 1 - M4_mean / (3 * M2_mean * M2_mean));
}

ArrayXXi randoms(int Nx, int Ny)
{
	mt19937 gen;
	uniform_int_distribution<> dist(0, 1);
	return ArrayXXi(Nx, Ny).unaryExpr([&gen, &dist](int x) { return 2 * dist(gen) - 1; });
}

int main()
{
	Eigen::initParallel();

	const vector<int> L = {100, 120, 140, 160, 180};

	const double Tmin = 1;
	const double Tmax = 4;
	const unsigned int TN = 100;
	const double Th = (Tmax - Tmin) / TN;

	const int sweeps = 1e5;
	const int equiSweeps = 1e3;
	const int O = 1e2;
	const int P = 1e2;

	vector<vector<double>> C(L.size(), vector<double>(TN + 1));
	vector<vector<double>> M(L.size(), vector<double>(TN + 1));
	vector<vector<double>> varM(L.size(), vector<double>(TN + 1));
	vector<vector<double>> binder(L.size(), vector<double>(TN + 1));

	for(unsigned int i = 0; i < L.size(); i++)
	{
		#pragma omp parallel for
		for(unsigned int j = 0; j <= TN; j++)
		{
			tie(C[i][j], M[i][j], varM[i][j], binder[i][j]) = mc(Tmin + Th * j, randoms, L[i], L[i], sweeps, equiSweeps, O, P);
		}
		cout << L[i] << endl;
	}

	for(unsigned int i = 0; i < L.size(); i++)
	{
		char name[256];
		sprintf(name, "L-%i.dat", L[i]);
		auto fileCM = fopen(name, "w");
		for(unsigned int j = 0; j <= TN; j++)
		{
			fprintf(fileCM, "%.20f %.20f %.20f %.20f %.20f\n", Tmin + Th * j, C[i][j], M[i][j], varM[i][j], binder[i][j]);
		}
		fclose(fileCM);
	}
	return 0;
}
