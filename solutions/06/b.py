import sys

import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pgf import FigureCanvasPgf
mpl.backend_bases.register_backend('pdf', FigureCanvasPgf)

L = [100, 120, 140, 160, 180]
Tc = 2 / np.log(1 + np.sqrt(2))
β = 1 / 8
ν = 1

fig, ax = plt.subplots(1, 1)
fig2, ax2 = plt.subplots(1, 1)
for l in L:
    T, _, M, varM, _ = np.loadtxt('L-{}.dat'.format(l), unpack=True)
    ax.errorbar(T, np.abs(M), yerr=np.sqrt(varM), fmt='x', label='$L = {}$'.format(l))
    ax2.plot((T - Tc) / Tc * l**(1 / ν), np.abs(M) * l**(β / ν), 'x', label='$L = {}$'.format(l))
ax.legend(loc='best')
ax2.legend(loc='best')
fig.savefig('M.pdf')
fig2.savefig('M-collapse.pdf')
