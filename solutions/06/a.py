import sys

import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pgf import FigureCanvasPgf
mpl.backend_bases.register_backend('pdf', FigureCanvasPgf)

L = [100, 120, 140, 160, 180]

fig, ax = plt.subplots(1, 1)
for l in L:
    T, C, _, _, _ = np.loadtxt('L-{}.dat'.format(l), unpack=True)
    ax.plot(T, C, 'x', label='$L = {}$'.format(l))
ax.legend(loc='best')
fig.savefig('C.pdf')
